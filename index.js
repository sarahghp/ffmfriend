#!/usr/bin/env node
const { program } = require('commander');
const { commandsList } = require('./commands');

program.version('0.0.1');

Object.values(commandsList).forEach(({ command, description, action, options = [] }) => {
  const current = program
    .command(command)
    .description(description)
    .action(action)

  options.forEach((opt) => {
    current.option(opt);
  });

});

program.parse(process.argv);
