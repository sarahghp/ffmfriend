## A little wrapper around my most used ffmpeg actions

```
Usage: ffmfriend [options] [command]  

Options:
  -V, --version                                   output the version number
  -h, --help                                      display help for command

Commands:
  crop [options] <file> <x> <y> <width> <height>  crop file
  crop-preview <file> <x> <y> <width> <height>    preview cropped file
  format [options] <file>                         format file in a way apple and people like
  from-gif [options] <file>                       convert from gif to mp4
  resize [options] <file> <size> <length>         resize file to target size (in MB)
  scale [options] <file> <scaleBy>                scale file
  segment [options] <file> <start> <end>          extract segment of file
  to-gif [options] <file>                         convert to gif
  help [command]                                  display help for command
  
  Command options include -a for aac audio and -o for a custom output file name
  
  ```
  
  ### To use
  1. Clone repo
  2. `npm i` in root directory
  3. `npm link` in root directory
  
  NB: Feel free to use this but I make no promises about maintenance
  