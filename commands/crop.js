const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const cropFile = (file, x, y, width, height, opts) => {
  const outputFile = opts.output || `${basename(file, extname(file))}-crop.mp4`;
  
  const command =  `-i ${file} -filter:v crop=${width}:${height}:${x}:${y} ${outputFile}`;
  execFfCommand([['ffmpeg', command.split(' ')]]);
}

exports.crop = {
  command: 'crop <file> <x> <y> <width> <height>',
  options: ['-o, --output [output-file]'],
  description: 'crop file',
  action: checkAndExecute(cropFile),
};