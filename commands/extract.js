const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const extract = (file, ips, opts) => {
  const outputFile = opts.output || `${basename(file, extname(file))}-formatted.mp4`;
  const prefix = opts.prefix ? `${opts.prefix}-` : '';

  const command =  `-i ${file} -vf fps=${ips} ${prefix}%04d.png`;

  execFfCommand([['ffmpeg', command.split(' ')]]);

}

exports.extract = {
  command: 'extract <file> <ips>',
  options: ['-p, --prefix [file-prefix]'],
  description: 'extract images',
  action: checkAndExecute(extract),
};
