const { spawn } = require('child_process');
const { StringDecoder } = require('string_decoder');

const decoder = new StringDecoder('utf8');

exports.execFfCommand = (processes) => {

  let currentProcess;
   
  const startProcess = ([processName, args]) => {
   currentProcess = spawn(processName, args);
   process.stdin.pipe(currentProcess.stdin)
   
   currentProcess.stderr.on('data', (data) => console.log(decoder.write(data)));
   
   currentProcess.on('exit', () => {
     if (processes.length > 0) {
       const nextProcess = processes.shift();
       startProcess(nextProcess);
     }
     
     process.stdin.destroy();
   });
   
  } 
  
  const firstProcess = processes.shift();
  startProcess(firstProcess);
  
} 