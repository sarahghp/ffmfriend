const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const scaleFile = (file, scaleBy, opts) => {
  const audioCodec = opts.audio ? 'aac' : 'mp2';
  const outputFile = opts.output || `${basename(file, extname(file))}-scaled.mp4`;

  const command =  `-i ${file} -vcodec h264 -acodec ${audioCodec} -pix_fmt yuv420p -vf scale=trunc(iw*${scaleBy}):trunc(ih*${scaleBy}) ${outputFile}`;
  
  execFfCommand([['ffmpeg', command.split(' ')]]);

}

exports.scale = {
  command: 'scale <file> <scaleBy>',
  options: ['-a, --audio', '-o, --output [output-file]'],
  description: 'scale file',
  action: checkAndExecute(scaleFile),
};