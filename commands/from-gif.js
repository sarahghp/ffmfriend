const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const makeGif = (file, opts) => {
  console.log(opts.output);
  const output = opts.output || `${basename(file, extname(file))}.mp4`;

  const command = `-i ${file} -movflags faststart -pix_fmt yuv420p -vf scale=trunc(iw/2)*2:trunc(ih/2)*2 ${output}`;
  
  execFfCommand([['ffmpeg', command.split(' ')]]);

}

exports.fromGif = {
  command: 'from-gif <file>',
  options: ['-o, --output [output-file]'],
  description: 'convert from gif to mp4',
  action: checkAndExecute(makeGif),
};