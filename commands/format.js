const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const formatFile = (file, opts) => {
  const audioCodec = opts.audio ? 'aac' : 'mp2';
  const outputFile = opts.output || `${basename(file, extname(file))}-formatted.mp4`;

  const command =  `-i ${file} -vcodec h264 -acodec ${audioCodec} -pix_fmt yuv420p ${outputFile}`;
  
  execFfCommand([['ffmpeg', command.split(' ')]]);

}

exports.format = {
  command: 'format <file>',
  options: ['-a, --audio', '-o, --output [output-file]'],
  description: 'format file in a way apple and people like',
  action: checkAndExecute(formatFile),
};