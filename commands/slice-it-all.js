const { basename, extname, parse, join } = require('path');
const { existsSync, lstatSync, mkdirSync, readdirSync, unlinkSync } = require('fs');
const { execFfCommand } = require('./execute');

const isDir = (path) => {
  return existsSync(path) && lstatSync(path).isDirectory();
};

const segment = (file, start, dur, outputFile, opts) => {
  const audioCodec = opts.audio ? 'aac' : 'mp2';

  const command =  `-i ${file} -vcodec h264 -acodec ${audioCodec} -pix_fmt yuv420p -ss ${start} -t 00:00:0${dur} ${outputFile}`;
  
  // console.log(command);
  
  execFfCommand([['ffmpeg', command.split(' ')]]);

}

const sliceItAll = (file, lengthIn, opts) => {

  const dur = opts.duration || .5; // duration in seconds
  const fileDurationInMs = lengthIn * 1000;
  const numberOfSegments = Math.floor(fileDurationInMs / (dur * 1000));
  const { dir } = parse(file);
  const fileName = basename(file, extname(file));
  const sliceDir = join(dir, `${fileName}-slices`);
  
  if (!isDir(sliceDir)) {
    mkdirSync(sliceDir);
  } else {
    try {
      const oldFiles = readdirSync(sliceDir);
      
      for (const file of oldFiles) {
        unlinkSync(join(sliceDir, file))
      }
    } catch (err) {
      console.error('From file delete:')
      console.error(err)
    }
  }
  
  for (var i = 0; i < numberOfSegments; i++) {
    const outputFile = `${sliceDir}/${fileName}-segment-${i}.mp4`;
    const start = dur * i;
    segment(file, start, dur, outputFile, opts);
  }

}

exports.sliceItAll = {
  command: 'slice-it-all <file> <lengthIn>',
  options: ['-a, --audio', '-d, --duration [val], -g, --gap [val]'],
  description: 'slice',
  action: sliceItAll,
};