const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const fileToMov = (file, opts) => {

  const outputFile = opts.output || `${basename(file, extname(file))}.mov`;
  
  const command =  `-i ${file} -acodec copy -vcodec copy -f mov ${outputFile}`;
  execFfCommand([['ffmpeg', command.split(' ')]]);
}

exports.toMov = {
  command: 'to-mov <file>',
  options: ['-o, --output [output-file]'],
  description: 'convert containter to mov',
  action: checkAndExecute(fileToMov),
};