const { basename, extname } = require('path');
const { execFfCommand } = require('./execute');

const cropPreview = (file, x, y, width, height, opts) => {  
  const command =  `-i ${file} -vf crop=${width}:${height}:${x}:${y}`;
  execFfCommand([['ffplay', command.split(' ')]]);
}

exports.cropPreview = {
  command: 'crop-preview <file> <x> <y> <width> <height>',
  description: 'preview cropped file',
  action: cropPreview,
};