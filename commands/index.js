/**
  Commands to make:
    - target file size ☁︎
    - mov to mp4 (audio / no audio) ☁︎ >> format for apple ☁︎
    - crop ☁︎
    - scale ☁︎
    - extract section ☁︎
    - mov/mp4 to gif ☁︎
    - gif to mp4 ☁︎
    - preview crop  ☁︎
    - be able to ask questions ☁︎
    - do for single file and for directory
**/

const { crop } = require ('./crop');
const { cropPreview } = require ('./crop-preview');
const { extract } = require ('./extract');
const { format } = require ('./format');
const { fromGif } = require('./from-gif');
const { resize } = require ('./resize');
const { scale } = require ('./scale');
const { segment } = require ('./segment');
const { shuffle } = require('./shuffle');
const { sliceItAll } = require('./slice-it-all');
const { toGif } = require('./to-gif');
const { toMov } = require('./mov-me');

exports.commandsList = {
  crop,
  cropPreview,
  extract,
  format,
  fromGif,
  resize,
  scale,
  segment,
  shuffle,
  sliceItAll,
  toGif,
  toMov,
};
