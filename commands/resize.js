const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const convertToSeconds = (time) => {
  return time
    .split(':')
    .map((n) => +n)
    .reduceRight((acc, time, idx) => acc + (60 * (idx + 1) * time))
};

const convertToTaget = (file, size, length, opts) => {
  const bitrateRaw = size * 8192 / convertToSeconds(length);
  const birateWithAudio = opts.audio ? Math.floor(bitrateRaw - 128) : Math.floor(bitrateRaw);
  const audioOpts = opts.audio ? `-c:a aac -b:a 128k` : `-c:a mp2`;
  const outputFile = opts.output || `${basename(file, extname(file))}-sm.mp4`;
  
  const commands = [
    [
      'ffmpeg',
      `-y -i ${file} -c:v libx264 -b:v ${birateWithAudio}k -pass 1 -an -f mp4 /dev/null`.split(' ')
    ],
    [
      'ffmpeg',
      `-i ${file} -c:v libx264 -b:v ${birateWithAudio}k -pass 2 ${audioOpts} -preset slower ${outputFile}`.split(' ')
    ],
  ];
  
  execFfCommand(commands);

};

exports.resize = {
  command: 'resize <file> <size> <length>',
  options: ['-a, --audio', '-o, --output [output-file]'],
  description: 'resize file to target size (in MB)',
  action: checkAndExecute(convertToTaget),
};