const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const segmentFile = (file, start, end, opts) => {
  const audioCodec = opts.audio ? 'aac' : 'mp2';
  const outputFile = opts.output || `${basename(file, extname(file))}-segment.mp4`;

  const command =  `-i ${file} -vcodec h264 -acodec ${audioCodec} -pix_fmt yuv420p -ss ${start} -to ${end} ${outputFile}`;
  
  execFfCommand([['ffmpeg', command.split(' ')]]);

}

exports.segment = {
  command: 'segment <file> <start> <end>',
  options: ['-a, --audio', '-o, --output [output-file]'],
  description: 'extract segment of file',
  action: segmentFile,
};