const { basename, extname } = require('path');
const { checkAndExecute } = require('../utils');
const { execFfCommand } = require('./execute');

const makeGif = (file, opts) => {
  const outputFile = opts.output || `${basename(file, extname(file))}.gif`;
  const commands = [
    [
      'ffmpeg',
      `-y -i ${file} -vf palettegen palette.png`.split(' '),
    ],
    [
      'ffmpeg',
      `-y -i ${file} -i palette.png -filter_complex paletteuse -r 10 ${outputFile}`.split(' '),
    ],
  ];
  
  execFfCommand(commands);

}

exports.toGif = {
  command: 'to-gif <file>',
  options: ['-o, --output [output-file]'],
  description: 'convert to gif',
  action: checkAndExecute(makeGif),
};