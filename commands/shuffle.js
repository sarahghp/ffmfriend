const { basename, extname, format, parse, join } = require('path');
const { appendFileSync, existsSync, lstatSync, mkdirSync, readdirSync, writeFileSync } = require('fs');
const { execFfCommand } = require('./execute');

const isDir = (path) => {
  return existsSync(path) && lstatSync(path).isDirectory();
};


// combine randomly until target time is reached 
// to combine write filenames to text file and then call
//  ffmpeg -f concat -safe 0 -i mylist.txt -c copy output.mp4

const shuffle = (dir, name, targetTime, opts) => {
  
  const audioCodec = opts.audio ? 'aac' : 'mp2';
  const outputFile = `${name}-shuffled.mp4`;
  
  const fileList = readdirSync(dir).filter(el => extname(el) === '.mp4');
  const clipFile = format({ dir, name, ext: '.txt'});
  writeFileSync(clipFile, `file ${fileList[0]} \n`);
  
  const dur = opts.duration || .5;
  const fileDurationInMs = targetTime * 1000;
  const numberOfSegments = Math.floor(fileDurationInMs / (dur * 1000));
    
  for (var i = 0; i < numberOfSegments; i++) {
    const segment = Math.floor(Math.random() * fileList.length);
    appendFileSync(clipFile, `file ${fileList[segment]} \n`)
  }
  
  const command =  `-f concat -safe 0 -i ${clipFile} -vcodec h264 -acodec ${audioCodec} -pix_fmt yuv420p ${outputFile}`;
  
  execFfCommand([['ffmpeg', command.split(' ')]]);

}

exports.shuffle = {
  command: 'shuffle-it-all <dir> <name> <targetTime>',
  options: ['-a, --audio', '-d, --duration [val]'],
  description: 'assemble',
  action: shuffle,
};