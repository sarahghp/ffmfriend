const { existsSync, lstatSync, readdirSync } = require('fs');

const isDir = (path) => {
  return existsSync(path) && lstatSync(path).isDirectory();
};

const getFileList = (path) => {
  return readdirSync(path);
}

const checkAndExecute = (fn) => (path, ...rest) => {
    
    if (isDir(path)) {
      getFileList(path).forEach((file) => {
        fn(`${path}/${file}`, ...rest);
      });
      
      return;
    }
    
    fn(path, ...rest);
    
}

module.exports = {
  checkAndExecute,
  isDir,
  getFileList
}